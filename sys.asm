setup_sys:
	call setup_keyboard
	call setup_user
	ret

; int 0x20 is user interrupt
setup_user:
	mov word [0x0082], 0
	mov word [0x0080], sys_user
	ret

; int 0x09
setup_keyboard:
	mov word [0x0026], 0
	mov word [0x0024], sys_kb
	ret

sys_user:
	mov ax, bx
	call print
	iret

sys_kb:
	cli
	push es
	push ax
	push bx
	xor ax, ax
	in al, 0x60
	mov [kb_input_scan], al
	mov bl, al
	cmp al, 0x2A | 128
	je .shiftunset
	cmp al, 0x2A
	je .shiftset
	and bl, 128
	cmp bl, 128
	je .end
	mov bl, [kb_shift]
	or bl, bl
	jnz .shift
	add ax, scan_table
._ret:
	mov bx, ax
	mov bl, [bx]
	mov [kb_input_buff], bl
	mov [kb_input_ready], byte 1
.end:
	mov al, 0x20
	out 0x20, al
	pop bx
	pop ax
	pop es
	sti
	iret
.shiftset:
	mov [kb_input_ready], byte 0
	mov [kb_shift], byte 1
	jmp .end
.shiftunset:
	mov [kb_input_ready], byte 0
	mov [kb_shift], byte 0
	jmp .end
.shift:
	add ax, shift_table
	jmp ._ret

kb_shift: db 0
kb_input_ready: db 0
kb_input_buff: db 0
kb_input_scan: db 0

